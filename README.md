# Clase 2

Materiales para la segunda clase, en esta clase cubrimos:

- Introducción a la programación funcional en Java y revisión de los elementos de soporte de Java

## Lista de Materiales

- [Apunte 03 - Programación funcional](./funcional/README.md)

## Lista de Ejemplos

- [Ejemplos de programación funcional](./funcional/)

## Estado general de la semana

Versión para publicación 2023.

***

## Software requerido

- Java
- Maven
- IntelliJ idea

## Clonar el presente repositorio

``` bash
cd existing_repo
git remote add origin https://gitlab.com/felisteffo.dev/arg-programa/clase-2/lectura-2.git
git branch -M main
git push -uf origin main
```

## Autores

Felipe Steffolani -
basado en un material original de la cátedra de Backend generado por Diego Serrano.

## License

Este trabajo está licenciado bajo una Licencia Creative Commons Atribución-NoComercial-CompartirIgual 4.0 Internacional. Para ver una copia de esta licencia, visita [https://creativecommons.org/licenses/by-nc-sa/4.0/deed.es](!https://creativecommons.org/licenses/by-nc-sa/4.0/deed.es).
