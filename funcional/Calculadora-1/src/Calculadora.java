import java.util.Scanner;

public class Calculadora {

    public static void main(String[] args) {

        Operacion[] operaciones;

        operaciones = new Operacion[]{
                new Suma(),
                new Resta(),
                new Multiplicacion(),
                new Division()
        };

        Scanner scanner = new Scanner(System.in);
        float a, b;
        int opcion;
        System.out.println("Calculadora básica");
        System.out.println("Seleccione una operación:");
        System.out.println("1. Suma");
        System.out.println("2. Resta");
        System.out.println("3. Multiplicación");
        System.out.println("4. División");
        System.out.println("0. Salir");
        opcion = scanner.nextInt();
        while (opcion != 0) {
            if (opcion >= 1 && opcion <= 4) {
                System.out.print("Ingrese el primer número: ");
                a = scanner.nextFloat();
                System.out.print("Ingrese el segundo número: ");
                b = scanner.nextFloat();
                float resultado = operaciones[opcion - 1].calcular(a, b);
                System.out.println("El resultado es: " + resultado);
            } else {
                System.out.println("Opción inválida. Intente nuevamente.");
            }
            System.out.println();
            System.out.println("Seleccione una operación:");
            opcion = scanner.nextInt();
        }
    }

}