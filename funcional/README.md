
# Apunte 03 - Programación funcional

## Introducción a la programación funcional

Al desarrollar software se identifican paradigmas de programación que gobiernan
el enfoque con el que se encara el problema, se diseña una solución y finalmente
se redacta el código fuente. Entre los paradigmas más utilizados se destacan el
paradigma estructurado y el paradigma orientado a objetos.

Otro paradigma de uso habitual es el paradigma funcional. El mismo tiene una
base matemática bastante compleja y por tal motivo existen pocos lenguajes
denominados puros, es decir, que permiten únicamente desarrollar con este
paradigma. Sin embargo, actualmente todos los lenguajes de uso cotidiano (tales
como Java, C#, Python o Javascript) incluyen ciertos elementos de esta metodología.

A pesar de que estos lenguajes están mayormente concentrados en los paradigmas
estructurado y orientado a objetos, ofrecen algunas características de
programación funcional. Esto se logra mediante la posibilidad de trabajar con un
nuevo tipo de dato denominado "función".

El origen de esta perspectiva puede parecer desconcertante, pero se la puede
analizar desde el punto de vista de las variables. Cuando se utiliza la
programación estructurada, los programas poseen variables donde se almacenan
valores. En esta modalidad, cada variable tiene la capacidad de almacenar
un único valor de alguno de los tipos de datos ofrecidos por el lenguaje,
tales como números, cadenas, booleanos, etc. En cambio, en la programación
orientada a objetos, las variables pueden contener objetos, los cuales
pueden tener múltiples atributos y métodos. Aunque es válido argumentar
que las variables solo contienen referencias a objetos, en esencia, sigue
siendo cierto que cada variable almacena un objeto.

En la programación funcional, las variables almacenan código fuente, es decir,
conjuntos de instrucciones del lenguaje. Por lo tanto, si una variable se
declara como una función, es posible ejecutar el código que esta contiene.
Y dado que son variables pueden cambiar su contenido, por lo tanto, si en
algún momento se asigna un código diferente a la variable y se ejecuta su
contenido nuevamente, se llevará a cabo una ejecución distinta. En conclusión,
siempre se ejecutará el conjunto de instrucciones que esté almacenado en la
variable en ese momento específico.

# Funciones de orden superior

Los lenguajes que ofrecen programación funcional, incluso en su forma más
básica, nos brindan la posibilidad de tener variables cuyo contenido sea una
función. También podemos tener parámetros de tipo función, lo cual implica que
un método puede ejecutar una función sin necesidad de saber con exactitud su
comportamiento. A pesar de parecer bastante confuso, este desacoplamiento es muy
utilizado gracias a las interfaces de Java.

De la misma manera, un método puede retornar una función para que sea almacenada
desde la llamada al mismo.

Los métodos que poseen parámetros o retorno de tipo función se denominan métodos
o funciones de orden superior. Esta noción tiene una gran relevancia matemática
y es muy útil en diversas situaciones de programación.

# Interfaces funcionales

La principal dificultad que se evidencia en un lenguaje orientado a objetos
y fuertemente tipado como es Java radica en que su sintaxis no es flexible para
un dato de tipo función. Al ser orientado a objetos, se debe trabajar con
clases, objetos y métodos.

Java logra ofrecer variables de tipo función mediante el concepto de las
interfaces funcionales. Una interfaz funcional es una interfaz que posee un
único método, la cual puede estar opcionalmente anotada como @FuncionalInterface.

Esta anotación no impide su uso para programación funcional, pero previene del
potencial error de que se le agregue un segundo método. En ese caso, al disponer
de más de un método deja de considerarse una interfaz funcional y todas las
clases que la implementen dejan de compilar correctamente. Si la interfaz está
anotada como funcional, el agregado de un segundo método es detectado por el
compilador como un error en la interfaz en lugar de marcar como erróneas las
clases implementadoras.

Como vimos, la condición para cumplir con el concepto de interfaz funcional es que la misma tenga un único método abstracto. El compilador de Java, si no le indicamos de alguna forma, no sabrá que estamos queriendo definir una interfaz funcional. Entonces, si tenemos una interfaz que la queremos usar como funcional, pero accidentalmente se agrega más de un método abstracto, la aplicación compilará correctamente. Pero cuando se quiera hacer uso, en tiempo de ejecución, de la interfaz funcional, la aplicación se romperá.

```java
@FunctionalInterface
public interface Suma {
    int sumar(int a, int b);
    int sumar(int a, int b, int c);
}
```

Con el uso de la anotación, que se agrega en el ejemplo, se puede prevenir este error en tiempo de ejecución. De esta forma el compilador, al momento de compilar la aplicación, si la interfaz tuviera más de un método abstracto, la misma no compilará, como se puede apreciar en la imagen

![picture 0](../images/ecf8d60679019f65ade3bdd505bcec1ed3771dacaefcaecdb99c6f16e358aaba.png)  

## Caso 1: Calculadora con orientación a objetos

Se presenta a continuación una implementación de una calculadora básica utilizando
POO para crear una calculadora básica. Los requerimientos iniciales de la misma
se resumen en que el usuario pueda indicar una operación y los dos operandos
numéricos con los que se desea calcular. Se desea que se puedan agregar nuevas
operaciones sin afectar el código existente.

Para ello se define una interfaz llamada "Operacion" con un método "calcular"
el cual toma dos operandos y devuelve el resultado. Esta interfaz representará
las operaciones básicas de la calculadora.

```java
@FunctionalInterface
public interface Operacion {
    float calcular(float a, float b);
}
```

A continuación, por cada operación que la calculadora ofrece se agrega
una clase que implemente la interfaz Operacion. Cada clase proporciona su
propia implementación del método "calcular".

```java
public class Suma implements Operacion {
    @Override
    public float calcular(float a, float b) {
        return a + b;
    }
}

public class Resta implements Operacion {
    @Override
    public float calcular(float a, float b) {
        return a - b;
    }
}

public class Multiplicacion implements Operacion {
    @Override
    public float calcular(float a, float b) {
        return a * b;
    }
}

public class Division implements Operacion {
    @Override
    public float calcular(float a, float b) {
        if (b != 0) {
            return a / b;
        } else {
            throw new ArithmeticException("No se puede dividir entre cero.");
        }
    }
}
```

Finalmente en la clase principal se crea una instancia de cada una de las operaciones
y se las almacena en alguna estructura de datos, tal como un arreglo.

```java
import java.util.Scanner;

public class Calculadora {

    public static void main(String[] args) {

        Operacion[] operaciones;

        operaciones = new Operacion[]{
                new Suma(),
                new Resta(),
                new Multiplicacion(),
                new Division()
        };

        Scanner scanner = new Scanner(System.in);
        float a, b;
        int opcion;
        System.out.println("Calculadora básica");
        System.out.println("Seleccione una operación:");
        System.out.println("1. Suma");
        System.out.println("2. Resta");
        System.out.println("3. Multiplicación");
        System.out.println("4. División");
        System.out.println("0. Salir");
        opcion = scanner.nextInt();
        while (opcion != 0) {
            if (opcion >= 1 && opcion <= 4) {
                System.out.print("Ingrese el primer número: ");
                a = scanner.nextFloat();
                System.out.print("Ingrese el segundo número: ");
                b = scanner.nextFloat();
                float resultado = operaciones[opcion - 1].calcular(a, b);
                System.out.println("El resultado es: " + resultado);
            } else {
                System.out.println("Opción inválida. Intente nuevamente.");
            }
            System.out.println();
            System.out.println("Seleccione una operación:");
            opcion = scanner.nextInt();
        }
    }

}
```

Esta implementación a pesar de cumplir con todos los requerimientos impone la
dificultad de crear una clase por cada operación con el único objetivo de
brindar una sobreescritura al método calcular.

## Funciones lambda

Las extensiones funcionales de Java permiten implementar interfaces funcionales
sin requerir la creación explícita de una nueva clase mediante las expresiones
lambda.

Dado que una interfaz funcional posee un único método, resulta redundante programar
una nueva clase con una sobreescritura. Si se declara una variable de referencia
a dicha interfaz, es inevitable que debe referenciar a una nueva implementación
que sobreescriba es único método. Las expresiones lambda ofrecen la posibilidad
de escribir únicamente el bloque de código del método directamente en el lugar
de la declaracion de la variable, el cual es considerado automáticamente como
la sobreescritura del método.

Una expresión lambda tiene la siguiente sintaxis

```java
    (parámetros) -> { cuerpo }
```

De esta manera, las operaciones de la calculadora pueden ser reescritas de una
manera mucho más concisa y sin programar una clase nueva por cada una, siendo
cada una de las expresiones lambda una sobreescritura del método calcular:

```java
    Operacion suma = (float a, float b) -> { return a + b; };
    Operacion resta = (float a, float b) -> { return a - b; };
    Operacion multiplicacion = (float a, float b) -> { return a * b; };
    Operacion division = (float a, float b) -> {
        if (b != 0) {
            return a / b;
        } else {
            throw new ArithmeticException("No se puede dividir entre cero.");
        }
    };
    operaciones = new Operacion[] { suma, resta, multiplicacion, division };
```

Las expresiones lambda pueden omitir los paréntesis en el caso de poseer
un único parámetro. Y de la misma manera si su cuerpo consiste en una única
instrucción return pueden ser omitidas las llaves y la palabra clave return.

Entonces las funciones suma, resta y multiplicación pueden ser aún más
simples:

```java
    Operacion suma = (float a, float b) -> a + b;
    Operacion resta = (float a, float b) -> a - b;
    Operacion multiplicacion = (float a, float b) -> a * b;
```

Finalmente para utilizar las funciones se invoca a su método calcular():

```java
    float resultado = suma.calcular(34,87);
```

## Interfaces funcionales provistas

La API de funciones de java ofrece una amplia cantidad de interfaces funcionales
para los casos más habituales. Se considera una práctica no recomendada la
creación de interfaces funcionales nuevas si ya existe en la API una equivalente.

Las diversas interfaces funcionales que provee el paquete java.util.function
se diferencian entre sí únicamente por la cantidad y tipo de parámetros y el tipo
de retorno. Existe una cantidad bastante grande de interfaces funcionales provistas, 
para diferentes combinaciones de tipos de parámetros y de retorno. La más destacas son:

| Nombre de la interfaz | Parámetros | Tipo de retorno | Uso |
|-----------------------|------------|----------------|------|
| `Supplier<T>`         | Ninguno    | `T`            |Generador de valores, se suele usar con Stream.generate() (más adelante)|
| `Consumer<T>`         | `T`        | `void`         |Recibe un valor pero no retorna nada, se suele utilizar para el método forEach()|
| `Predicate<T>`        | `T`        | `boolean`      |Dado un valor retorna un boolean, se suele utilizar con filter() |
| `Function<T, R>`      | `T`        | `R`            |Dado un objeto retorna uno diferente, probablemente de otro tipo, se usa con map() |
| `UnaryOperator<T>`    | `T`        | `T`            |Tambien para usar con map, cuando el tipo del retorno es el mismo que el parámetro |

En el momento de necesitar una variable de tipo función, únicamente se debe
analizar los tipos de los parámetros y del retorno y con ellos seleccionar
una de las interfaces funcionales de la tabla.

Para el caso de la calculadora, el método calcular recibe dos parámetros
float y retorna otro float. Por lo tanto las interfaces más adecuadas son
```BinaryOperator<Float>``` o ```DoubleBinaryOperator```

De esta manera, para el caso de la calculadora tampoco es requerida
la declaración de la interfaz Operacion, siendo adecuado crear las variables
como:


```java
    DoubleBinaryOperator suma = (double a, double b) -> a + b;
    DoubleBinaryOperator resta = (double a, double b) -> a - b;
    DoubleBinaryOperator multiplicacion = (double a, double b) -> a * b;
    DoubleBinaryOperator division = (double a, double b) -> {
        if (b != 0) {
            return a / b;
        } else {
            throw new ArithmeticException("No se puede dividir entre cero.");
        }
    };
```

Y dado que estas variables son utilizadas únicamente para llenar el arreglo
de operaciones, también se las puede omitir y agregar el bloque de cada una
al llenado del arreglo:

```java
    DoubleBinaryOperator []operaciones = {
        (double a, double b) -> a + b,
        (double a, double b) -> a - b,
        (double a, double b) -> a * b,
        (double a, double b) -> {
            if (b != 0) {
                return a / b;
            } else {
                throw new ArithmeticException("No se puede dividir entre cero.");
            }
        }
    };
```

## Ejemplos

- [Calculadora con clases](./Calculadora-1)
- [Calculadora con interfaz funcional propia](./Calculadora-2)
- [Calculadora con interfaz funcional provista](./Calculadora-3)
